package com.example.amori_hernan.evaluacion2;

import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Main extends AppCompatActivity implements Home.OnFragmentInteractionListener, frag1.OnFragmentInteractionListener, frag2.OnFragmentInteractionListener, frag3.OnFragmentInteractionListener{

    @Override

    // ------> integrantes : Hernán Parra - Amori Navarrete  <-------

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager0 = getSupportFragmentManager();
        FragmentTransaction transition1 = fragmentManager0.beginTransaction();
        Home home = new Home();
        transition1.replace(R.id.pantalla, home);
        transition1.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
